import 'package:method_get/user_model.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // String output = "No data";
  User user = null;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('User Data'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              (user != null) ? user.id + " | " + user.name : "Tidak ada data",
            ),
            ElevatedButton(
              onPressed: () {
                User.connectToAPI("3").then((value) {
                  user = value;
                  setState(() {
                    
                  });
                });
              },
              child: Text('GET'),
            )
          ],
        ),
      ),
    ));
  }
}
